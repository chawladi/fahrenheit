package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * Divjot Chawla
 * Midterm exam
 */

public class FahrenheitTest {

	@Test
	public void testIsValidCelciusToFahrenheitRegular() {
	int isValidConversion = Fahrenheit.fromCelcius(30);
		assertTrue("Invalid to Fahrenheit Conversion", isValidConversion == 86);
		
	}

	 @Test(expected=NumberFormatException.class)
	public void testIsValidCelciusToFahrenheitException() {
		int isValidConversion = Fahrenheit.fromCelcius(0);
		assertFalse("Invalid to Fahrenheit Conversion", isValidConversion == Integer.parseInt("A"));
	}
	
	@Test
	public void testIsValidCelciusToFahrenheitBoundaryIn() {
		int isValidConversion = Fahrenheit.fromCelcius(1);
		assertTrue("Invalid to Fahrenheit Conversion", isValidConversion == 34);
	}
	
	@Test(expected=NumberFormatException.class)
	public void testIsValidCelciusToFahrenheitBoundaryOut() {
		int isValidConversion = Fahrenheit.fromCelcius(0);
		assertFalse("Invalid to Fahrenheit Conversion", isValidConversion == 32);
	}
}
